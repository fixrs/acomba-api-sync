<?php
declare(strict_types=1);

namespace App\Domain\SiteOrder;

use JsonSerializable;

class SiteOrder implements JsonSerializable
{
    /**
     * @var array
     */
    private $data;

    /**
     * @param array    $p_data
     */
    public function __construct($p_data)
    {
        $this->data = $p_data;
    }

    /**
     * @return string
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->data;
    }
}
