<?php

namespace App\Domain\SiteOrder;

use App\Application\Misc\AcombaFormaters;
use App\Domain\SiteOrder\SiteOrder;
use App\Domain\SiteOrder\SiteOrderNotFoundException;
use Automattic\WooCommerce\Client as SiteWCApiClient;
use App\Application\SiteApi\Client as SiteApiClient;

/**
 * Repository.
 */
final class SiteOrderRepository
{
    /**
     * @var SiteWCApiClient The WooCommerce Api Client
     */
    private $wcClient;

    /**
     * @var SiteApiClient
     */
    protected $siteClient;

    /**
     * Constructor.
     *
     * @param SiteWCApiClient $p_client The WooCommerce Api Client
     */
    public function __construct(SiteWCApiClient $p_wcClient, SiteApiClient $p_siteClient)
    {
        $this->wcClient = $p_wcClient;
        $this->siteClient = $p_siteClient;
    }

    public function getSiteOrderById(int $p_id): SiteOrder
    {
        return new SiteOrder($this->wcClient->get('orders/'.$p_id));
    }

    /**
     * Constructor.
     *
     * @return SiteOrder[]
     */
    public function getNewSiteOrders(): array
    {
        return $this->siteClient->get('orders/new');
    }
}
