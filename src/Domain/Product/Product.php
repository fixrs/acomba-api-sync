<?php
declare(strict_types=1);

namespace App\Domain\Product;

use JsonSerializable;


// (CuNumber, CuName, CuActive, CuPostalCode,CuAddress,CuCity) 

class Product implements JsonSerializable
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $descriptionFr;

    /**
     * @var string
     */
    private $descriptionEn;

    /**
     * @var float
     */
    private $price1;

    /**
     * @var float
     */
    private $price2;

    /**
     * @var float
     */
    private $price3;

    /**
     * @var float
     */
    private $price4;

    /**
     * @var int
     */
    private $stock;

    /**
     * @param int|null  $id
     * @param string    $title
     */
    public function __construct(string $p_number, string $p_descriptionFr, ?string $p_descriptionEn, float $p_price1, float $p_price2, float $p_price3, float $p_price4, int $p_stock)
    {
        $this->number = $p_number;
        $this->descriptionFr = $p_descriptionFr;
        $this->descriptionEn = $p_descriptionEn;
        $this->price1 = $p_price1;
        $this->price2 = $p_price2;
        $this->price3 = $p_price3;
        $this->price4 = $p_price4;
        $this->stock = $p_stock;
    }

    /**
     * @return string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getDescription(string $p_lang = 'fr'): string
    {
        if ($p_lang == 'en') {
            return $this->descriptionEn;
        } else {
            return $this->descriptionFr;
        }
        
    }

    /**
     * @return float
     */
    public function getPrice(int $p_which = 1): float
    {
        if ($p_which > 0 && $p_which <= 4) {
            return $this->{'price'.$p_which};
        }

        return NULL;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'number' => $this->number,
            'description_fr' => $this->descriptionFr,
            'description_en' => $this->descriptionEn,
            'price1' => $this->price1,
            'price2' => $this->price2,
            'price3' => $this->price3,
            'price4' => $this->price4,
            'stock' => $this->stock
        ];
    }
}
