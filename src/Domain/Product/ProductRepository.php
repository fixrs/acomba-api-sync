<?php

namespace App\Domain\Product;

use App\Application\Misc\AcombaFormaters;
use App\Domain\Product\Product;
use App\Domain\Product\ProductNotFoundException;
use PDO;

/**
 * Repository.
 */
final class ProductRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $p_connection)
    {
        $this->connection = $p_connection;
    }

    /**
     * getAllActiveProducts
     *
     * @return Product[]
     */
    public function getAllActiveProducts(): array
    {
        $stmtProducts = $this->connection->prepare(
            'SELECT 
                PrNumber, 
                PrDescription1, PrDescription2, 
                PrSellingPrice0_1, PrSellingPrice0_2, PrSellingPrice0_3, PrSellingPrice0_4,
                PrQtyOnHand
            FROM Product WHERE PrActive <> 0'
        );

        if (!$stmtProducts->execute()) {
            throw new ProductNotFoundException();
        }

        $productList = array();
        while($acombaProduct = $stmtProducts->fetch()) {
            $acombaProduct = AcombaFormaters::iconvArrFromAcombaToUTF($acombaProduct);
            $productList[] = new Product(
                $acombaProduct['PrNumber'],
                $acombaProduct['PrDescription1'],
                $acombaProduct['PrDescription2'],
                (float) $acombaProduct['PrSellingPrice0_1'],
                (float) $acombaProduct['PrSellingPrice0_2'],
                (float) $acombaProduct['PrSellingPrice0_3'],
                (float) $acombaProduct['PrSellingPrice0_4'],
                (int) $acombaProduct['PrQtyOnHand']
            );
        }

        return $productList;
    }

    public function getActiveProductByNumber(string $p_number): Product
    {
        $stmtProduct = $this->connection->prepare(
            'SELECT 
                PrNumber, 
                PrDescription1, PrDescription2, 
                PrSellingPrice0_1, PrSellingPrice0_2, PrSellingPrice0_3, PrSellingPrice0_4,
                PrQtyOnHand
            FROM Product WHERE PrActive <> 0 AND PrNumber = :number'
        );
        $stmtProduct->bindValue(':number', $p_number);
        if (!$stmtProduct->execute()) {
            throw new ProductNotFoundException();
        }

        $Product = $stmtProduct->fetch(PDO::FETCH_ASSOC);
        if (!$Product) {
            throw new ProductNotFoundException();
        } 

        return new Product(
            $Product['PrNumber'], 
            $Product['PrDescription1'], 
            $Product['PrDescription2'], 
            $Product['PrSellingPrice0_1'], 
            $Product['PrSellingPrice0_2'],
            $Product['PrSellingPrice0_3'],
            $Product['PrSellingPrice0_4'],
            $Product['PrQtyOnHand']
        );
    }
}
