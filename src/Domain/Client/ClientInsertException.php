<?php
declare(strict_types=1);

namespace App\Domain\Client;

use App\Domain\DomainException\DomainRecordNotFoundException;

class ClientInsertException extends DomainRecordNotFoundException
{
    public $message = 'The client insertion failed';
}
