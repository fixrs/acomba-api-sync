<?php
declare(strict_types=1);

namespace App\Domain\Client;

use JsonSerializable;


// (CuNumber, CuName, CuActive, CuPostalCode,CuAddress,CuCity) 

class Client implements JsonSerializable
{
    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $name;


    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @param int|null  $id
     * @param string    $title
     */
    public function __construct(?int $p_phoneNumber, string $p_name, string $p_postalCode, string $_address, string $p_city)
    {
        $this->phoneNumber = $p_phoneNumber;
        $this->name = $p_name;
        $this->postalCode = $p_postalCode;
        $this->address = $_address;
        $this->city = $p_city;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->phoneNumber,
            'phone_number' => $this->phoneNumber,
            'name' => $this->name,
            'postal_code' => $this->postalCode,
            'address' => $this->address,
            'city' => $this->city
        ];
    }
}
