<?php

namespace App\Domain\Client;

use App\Application\Misc\AcombaFormaters;
use App\Domain\Client\Client;
use App\Domain\Client\ClientNotFoundException;
use PDO;

/**
 * Repository.
 */
final class ClientRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $p_connection)
    {
        $this->connection = $p_connection;
    }

    public function getClientById(int $p_id): Client
    {
        return $this->getClientByNumber($p_id);
    }

    public function getClientByPhone(string $p_phone): Client
    {
        return $this->getClientByNumber($p_phone);
    }

    public function getClientByNumber(int $p_number): Client
    {
        $p_number = AcombaFormaters::clientPhoneNumberId($p_number);

        $stmtCustomer = $this->connection->prepare(
            'SELECT * FROM Customer WHERE cuNumber = :id'
        );
        $stmtCustomer->bindValue(':id', $p_number);
        if (!$stmtCustomer->execute()) {
            throw new ClientNotFoundException();
        }

        $client = $stmtCustomer->fetch(PDO::FETCH_ASSOC);
        if (!$client) {
            throw new ClientNotFoundException();
        } 

        return new Client(
            $client['CuNumber'], 
            $client['CuName'], 
            $client['CuPostalCode'], 
            $client['CuAddress'], 
            $client['CuCity']
        );
    }

    public function createNewClient(Client $p_client): Client
    {
        $insertStmtCustomer = $this->connection->prepare(
            'INSERT INTO Customer 
                (CuNumber, CuName, CuActive, CuPostalCode,CuAddress,CuCity) 
            VALUES 
                ( :CuNumber, :CuName, :CuActive, :CuPostalCode, :CuAddress, :CuCity )'
        );
        $insertStmtCustomer->bindValue(':CuNumber',         $p_client->getPhoneNumber());
        $insertStmtCustomer->bindValue(':CuName',           $p_client->getName());
        $insertStmtCustomer->bindValue(':CuActive',         1);
        $insertStmtCustomer->bindValue(':CuPostalCode',     $p_client->getPostalCode());
        $insertStmtCustomer->bindValue(':CuAddress',        $p_client->getAddress());
        $insertStmtCustomer->bindValue(':CuCity',           $p_client->getCity());
        if (!$insertStmtCustomer->execute()) {
            throw new ClientInsertException();
        }
        
        return $this->getClientById($p_client->getPhoneNumber());
    }
}
