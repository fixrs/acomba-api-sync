<?php
declare(strict_types=1);

namespace App\Application\SiteApi;

class Client
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var token
     */
    protected $token;

    /**
     * Initialize client.
     *
     * @param string $p_url          WP/WC Site Side Api URL.
     * @param string $consumerKey    WP/WC Site Side Api Token.
     */
    public function __construct($p_url, $p_token)
    {
        $this->url = $p_url;
        $this->token = $p_token;
    }

    /**
     * GET method.
     *
     * @param string $endpoint   API endpoint.
     * @param array  $parameters Request parameters.
     *
     * @return array|object
     */
    public function get($p_endpoint)
    {
        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "X-Auth: ".$this->token."\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        $jsonResponse = json_decode(file_get_contents($this->url.'/'.$p_endpoint, false, $context), true);
        
        if ($jsonResponse['statusCode'] == 200) {
            return $jsonResponse['data'];
        } else {
            return NULL;
        }
    }
}