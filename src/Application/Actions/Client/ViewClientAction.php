<?php
declare(strict_types=1);

namespace App\Application\Actions\Client;

use Psr\Http\Message\ResponseInterface as Response;

class ViewClientAction extends ClientAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $clientId = $this->resolveArg('id');

        $this->logger->info("Client with WP id `${clientId}` was viewed.");
        
        $client = $this->clientRepository->getClientById($clientId);
        if ($client) {
            return $this->respondWithData($client);
        } else {
            return $this->respondWithData(NULL);
        }
    }
}
