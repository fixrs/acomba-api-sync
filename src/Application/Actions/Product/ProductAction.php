<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Action;
use App\Application\SiteApi\Client as SiteApiClient;
use App\Domain\Product\ProductRepository;
use Psr\Log\LoggerInterface;
use Automattic\WooCommerce\Client as SiteWCApiClient;

abstract class ProductAction extends Action
{
    /**
     * @var SiteApiClient
     */
    protected $SiteApiClient;

    /**
     * @var SiteWCApiClient
     */
    protected $SiteWCApiClient;

    /**
     * @var ProductRepository
     */
    protected $ProductRepository;

    /**
     * @param LoggerInterface $logger
     * @param SiteApiClient $SiteApiClient
     * @param SiteWCApiClient $SiteWCApiClient
     * @param ProductRepository $ProductRepository
     */
    public function __construct(
        LoggerInterface $logger,
        SiteApiClient $SiteApiClient,
        SiteWCApiClient $SiteWCApiClient,
        ProductRepository $ProductRepository
    ) {
        parent::__construct($logger);
        $this->SiteApiClient = $SiteApiClient;
        $this->SiteWCApiClient = $SiteWCApiClient;
        $this->ProductRepository = $ProductRepository;
    }
}
