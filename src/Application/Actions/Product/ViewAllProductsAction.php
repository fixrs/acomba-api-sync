<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;

class ViewAllProductsAction extends ProductAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->logger->info("Listed all active products.");
        
        $Products = $this->ProductRepository->getAllActiveProducts();
        if ($Products) {
            return $this->respondWithData($Products);
        } else {
            return $this->respondWithData(NULL);
        }
    }
}
