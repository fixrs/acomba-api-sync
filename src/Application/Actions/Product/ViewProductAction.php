<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;

class ViewProductAction extends ProductAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $ProductNumber = $this->resolveArg('number');

        $this->logger->info("Product with Acomba number `${ProductNumber}` was viewed.");
        
        $Product = $this->ProductRepository->getActiveProductByNumber($ProductNumber);
        if ($Product) {
            return $this->respondWithData($Product);
        } else {
            return $this->respondWithData(NULL);
        }
    }
}
