<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Domain\Product\Product;
use Psr\Http\Message\ResponseInterface as Response;

class SyncProductsWithSiteAction extends ProductAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dryRun = FALSE;
        $log_folder = './sync_logs/'.date('d-m-y_h-i-s').($dryRun ? '-DR' : '');
        mkdir($log_folder);
        
        // Get products that will be deleted
        $deletedProducts = [];
        /*$handle = fopen("products_to_delete.txt", "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if (strlen($line) > 2) {
                    $deletedProducts[] = trim($line);
                }
            }

            fclose($handle);
        } else {
            die('To be deleted products file missing');
        }*/

        $handle = fopen("mid_ops.txt", "r");
        $midOps = [];
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $siteMarkerPos = strpos($line, 'SITE :');
                $skuMarkerPos = strpos($line, 'SKU :');
                $siteSkuMarkerPos = strpos($line, '- -');
                $acombaSku = trim(substr($line, $skuMarkerPos+5));
                $site = trim(substr($line, $siteMarkerPos+6, $skuMarkerPos-$siteMarkerPos-6));;
                $siteSku = trim(substr($line, 0, $siteSkuMarkerPos));
                if ($acombaSku && $site != 'PARENT') {
                    $midOps[] = [
                        #'line'=>$line, 
                        'siteSku'=>$siteSku, 'site'=>$site, 'acombaSku'=>$acombaSku
                    ];
                }
            }

            fclose($handle);
        } else {
            die('Mid ops for SKUs file missing');
        } 
        
        // Get Products in Site DB
        $productsInSite = $this->SiteApiClient->get('products/all_skus_with_ids');
        file_put_contents($log_folder.'/products_in_site.json', json_encode($productsInSite));


        // Cleanup SKUs with MidOps
        foreach ($midOps as $currMidOp) {
            if (strlen($currMidOp['site']) && $currMidOp['acombaSku'] != 'DELETE') {
                foreach ($productsInSite as $i=>$siteProduct) {
                    if ($siteProduct['id'] == $currMidOp['site']) {
                        $productsInSite[$i]['sku'] = $currMidOp['acombaSku'];
                    }
                }
            }

            if (strlen($currMidOp['siteSku']) && $currMidOp['acombaSku'] != 'DELETE') {
                foreach ($productsInSite as $i=>$siteProduct) {
                    if ($siteProduct['sku'] == $currMidOp['siteSku']) {
                        $productsInSite[$i]['sku'] = $currMidOp['acombaSku'];
                    }
                }
            }
        }

        // Get groups in Site DB
        $userGroups = $this->SiteApiClient->get('groups');
        file_put_contents($log_folder.'/user_groups.json', json_encode($userGroups));
        
        // Get Products in Acomba
        $acombaProducts = $this->ProductRepository->getAllActiveProducts();
        #file_put_contents('./all_products_2.arr', serialize($acombaProducts)); exit;
        /*$acombaProductsCleaned = [];
        foreach ($acombaProducts as $acombaProduct) {
            if ($acombaProduct->getPrice(1) > 0 && $acombaProduct->getStock() > 0) {
                $acombaProductsCleaned[] = $acombaProduct;
            }
        }
        $acombaProducts = $acombaProductsCleaned;*/

        $assessmentArr = [
            'site_products'=>$productsInSite,
            'site_total'=>count($productsInSite), 
            'acomba_products'=>$acombaProducts,
            'acombaTotal'=>count($acombaProducts), 
            'deleted_site_products'=>$deletedProducts,
            'deleted'=>[], 
            'matches'=>[], 
            'nomatches'=>[]
        ];

        $uncleanedProductsInSite = $productsInSite;
        $productsInSite = [];
        foreach($uncleanedProductsInSite as $i=>$siteProduct) {
            if (in_array($siteProduct['sku'], $deletedProducts)) {
                $assessmentArr['deleted'][] = $siteProduct;
            } else {
                $productsInSite[] = $siteProduct;
            }
        }
        $assessmentArr['site_products'] = $productsInSite;
        $assessmentArr['site_total'] = count($productsInSite);

        foreach ($acombaProducts as $acombaProduct) {
            $matched = false;
            foreach($productsInSite as $siteProduct) {
                if ($acombaProduct->getNumber() == $siteProduct['sku']) {
                    $assessmentArr['matches'][] = [
                        'acomba'=>$acombaProduct,
                        'site'=>$siteProduct
                    ];
                    $matched = true;
                } else if ($acombaProduct->getNumber() == 'S'.$siteProduct['sku']) {
                    $assessmentArr['matches'][] = [
                        'acomba'=>$acombaProduct,
                        'site'=>$siteProduct
                    ];
                    $matched = true;
                }
            }
            
            if (!$matched) {
                $assessmentArr['nomatches'][] = $acombaProduct;
            }
            
        }

        // DUMP ASSESSMENT ARR CUTOFF
        #return $this->respondWithData($assessmentArr);

        // Loop thorugh Acomba products, if we have match update it, if not and it's not ignored create it
        $syncArr = ['update'=>[], 'variation_updates'=>[], 'create'=>[], 'delete'=>[]];


        foreach ($assessmentArr['matches'] as $matchedProduct) {
            if (intval($matchedProduct['site']['parent']) && $matchedProduct['site']['wp_type'] == 'product_variation') {
                $syncArr['variation_updates'][$matchedProduct['site']['parent']][] = [
                    'id' => $matchedProduct['site']['id'],
                    'price' => $matchedProduct['acomba']->getPrice(1),
                    'regular_price' => $matchedProduct['acomba']->getPrice(1),
                    'stock_quantity' => $matchedProduct['acomba']->getStock(),
                    'sku' => $matchedProduct['acomba']->getNumber(),
                    'meta_data' => $this->preparePricesGroupsMetaData($userGroups, $matchedProduct['acomba'])
                ];
            } else {
            $syncArr['update'][] = [
                'id' => $matchedProduct['site']['id'],
                'price' => $matchedProduct['acomba']->getPrice(1),
                'regular_price' => $matchedProduct['acomba']->getPrice(1),
                'stock_quantity' => $matchedProduct['acomba']->getStock(),
                'sku' => $matchedProduct['acomba']->getNumber(),
                'meta_data' => $this->preparePricesGroupsMetaData($userGroups, $matchedProduct['acomba'])
            ];
        }

        }

        foreach ($assessmentArr['nomatches'] as $unmatchedProduct) {
            $syncArr['create'][] = [
                'name' => $unmatchedProduct->getDescription(),
                'price' => $unmatchedProduct->getPrice(1),
                'regular_price' => $unmatchedProduct->getPrice(1),
                'stock_quantity' => $unmatchedProduct->getStock(),
                'sku' => $unmatchedProduct->getNumber(),
                'manage_stock' => true,
                'meta_data' => $this->preparePricesGroupsMetaData($userGroups, $unmatchedProduct),
                'status' => 'draft',
                'lang' => 'fr'
            ];
        }

        foreach ($assessmentArr['deleted'] as $deletedProduct) {
            $syncArr['delete'][] = $deletedProduct['id'];
        }

        // ADD MIDOPS DELETES
        /*foreach ($midOps as $currMidOp) {
            if (strlen($currMidOp['site']) > 0 && $currMidOp['acombaSku'] == 'DELETE') {
                $syncArr['delete'] = array_merge($syncArr['delete'], explode(';', $currMidOp['site']));
            }

            if (strlen($currMidOp['siteSku']) > 0 && $currMidOp['acombaSku'] == 'DELETE') {
                foreach ($productsInSite as $i=>$siteProduct) {
                    if ($siteProduct['sku'] == $currMidOp['siteSku']) {
                        $syncArr['delete'][] = $siteProduct['id'];
                    }
                }
            }
        }*/

        $maxPerRequest = 100;
        $syncArrChunked = [];
        $syncArrChunked['update'] = array_chunk($syncArr['update'], $maxPerRequest);
        $syncArrChunked['create'] = array_chunk($syncArr['create'], $maxPerRequest);
        $syncArrChunked['delete'] = array_chunk($syncArr['delete'], $maxPerRequest);

        $syncArrPaged = [];
        foreach ($syncArrChunked as $syncType=>$syncChunk) {
            foreach ($syncChunk as $currChunk) {
                $syncArrPaged[] = [$syncType=>$currChunk];
            }
        }

        $syncResponse = [];
        if (!$dryRun) {
        foreach ($syncArrPaged as $currSyncPage) {
                $syncResponse['products'][] = $this->SiteWCApiClient->post('products/batch', $currSyncPage); 
        }

            foreach ($syncArr['variation_updates'] as $parentProductId=>$currVariationBatchUpdate) {
                $syncResponse['variations'][] = $this->SiteWCApiClient->post('products/'.$parentProductId.'/variations/batch', ['update'=>$currVariationBatchUpdate]); 
            }
        }
        

        $this->logger->info("Products synched with Site.");

        $variationUpdatesCount = 0;
        foreach ($syncArr['variation_updates'] as $variationUpdate) {
            $variationUpdatesCount+= count($variationUpdate);
        }

        file_put_contents($log_folder.'/sync_arr.json', json_encode($syncArr));

        $finalResponse = [
            'update_count'=>count($syncArr['update']),
            'variation_updates_count'=>$variationUpdatesCount,
            'create_count'=>count($syncArr['create']),
            'delete_count'=>count($syncArr['delete']),
            'response'=>$syncResponse,
            'full_report'=>$syncArr, 
            #'paged'=>$syncArrPaged,
        ];

        unset($finalResponse['full_report']);

        file_put_contents($log_folder.'/summary.json', json_encode($finalResponse));

        return $this->respondWithData($finalResponse);
    }

    private function preparePricesGroupsMetaData($p_groupsData, Product $p_acombaProduct) {
        $metaData = [
            [
                'key' => '_acomba_price_2',
                'value' => $p_acombaProduct->getPrice(2)
            ],
            [
                'key' => '_acomba_price_3',
                'value' => $p_acombaProduct->getPrice(3)
            ],
            [
                'key' => '_acomba_price_4',
                'value' => $p_acombaProduct->getPrice(4)
            ],
            [
                'key' => '_groupes',
                'value' => 'field_61096c54e589b'
            ]
        ];

        /*$associatedGroups = [];
        $groupPrices = [];
        
        foreach ($p_groupsData as $groupData) {
            $intPriceIndex = intval($groupData['price_index']);
            if ($intPriceIndex === 1) {
                $associatedGroups[] = $groupData['id'];
            } else if ($intPriceIndex > 1 && $p_acombaProduct->getPrice($intPriceIndex)) {
                $associatedGroups[] = $groupData['id'];
                $groupPrices[$groupData['id']] = number_format($p_acombaProduct->getPrice($intPriceIndex), 2, '.', '');
            }
        }

        if (count($associatedGroups) > 0) {
            $metaData[] = [
                'key'=>'groupes',
                'value'=>$associatedGroups
            ];
        }

        if (count($groupPrices) > 0) {
            $metaData[] = [
                'key'=>'_user_group_prices',
                'value'=>(object)$groupPrices
            ];
        }*/

        return $metaData;
    }
}
