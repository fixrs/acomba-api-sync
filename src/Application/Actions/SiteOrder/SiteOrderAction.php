<?php
declare(strict_types=1);

namespace App\Application\Actions\SiteOrder;

use App\Application\Actions\Action;
use App\Domain\SiteOrder\SiteOrderRepository;
use Psr\Log\LoggerInterface;

abstract class SiteOrderAction extends Action
{
    /**
     * @var SiteOrderRepository
     */
    protected $SiteOrderRepository;

    /**
     * @param LoggerInterface $logger
     * @param SiteOrderRepository $SiteOrderRepository
     */
    public function __construct(
        LoggerInterface $logger,
        SiteOrderRepository $SiteOrderRepository
    ) {
        parent::__construct($logger);
        $this->SiteOrderRepository = $SiteOrderRepository;
    }
}
