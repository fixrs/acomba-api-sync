<?php
declare(strict_types=1);

namespace App\Application\Actions\SiteOrder;

use Psr\Http\Message\ResponseInterface as Response;

class ViewNewSiteOrdersAction extends SiteOrderAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->logger->info("Got new site orders.");
        
        return $this->respondWithData(
            //$this->SiteOrderRepository->getNewSiteOrders()
            [4285, 4286]
        );
    }
}
