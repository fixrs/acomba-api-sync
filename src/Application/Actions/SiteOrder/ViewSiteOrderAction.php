<?php
declare(strict_types=1);

namespace App\Application\Actions\SiteOrder;

use Psr\Http\Message\ResponseInterface as Response;

class ViewSiteOrderAction extends SiteOrderAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $SiteOrderId = (int) $this->resolveArg('id');

        $this->logger->info("SiteOrder with WP id `${SiteOrderId}` was viewed.");
        
        $SiteOrder = $this->SiteOrderRepository->getSiteOrderById($SiteOrderId);
        if ($SiteOrder) {
            return $this->respondWithData($SiteOrder);
        } else {
            return $this->respondWithData(NULL);
        }
    }
}
