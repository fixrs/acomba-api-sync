<?php
declare(strict_types=1);

namespace App\Application\Misc;

class AcombaFormaters
{
    static function postalCode($p_postalCode) {
        $treatedPostalCode = (string)$p_postalCode;
        $treatedPostalCode = preg_replace('/[^0-9A-Z]/i', '', strtoupper($p_postalCode));
        if (preg_match('/^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/', $treatedPostalCode) === 1) { // CA Postal Code
            $treatedPostalCode = substr_replace($treatedPostalCode, ' ', 3, 0);
        } elseif (preg_match('/^[0-9]{5}(?:-[0-9]{4})?$/', $treatedPostalCode) === 1) { // USA Zip code
            $treatedPostalCode = $treatedPostalCode;
        } elseif (strlen($treatedPostalCode) != 6) {
            $treatedPostalCode = '';
        }
        
        return $treatedPostalCode;
    }

    static function clientPhoneNumberId($p_phoneNumber) {
        $treatedPhone = (string)$p_phoneNumber;
        $treatedPhone = preg_replace('/[^0-9]/i', '', $treatedPhone);
        if (strlen($treatedPhone) == 11) {
            $treatedPhone = substr_replace($treatedPhone, '-', 1, 0);
            $treatedPhone = substr_replace($treatedPhone, '-', 5, 0);
            $treatedPhone = substr_replace($treatedPhone, '-', 9, 0);
        }    
        elseif (strlen($treatedPhone) > 6 && strlen($treatedPhone) <= 10) {
            $treatedPhone = substr_replace($treatedPhone, '-', 3, 0);
            $treatedPhone = substr_replace($treatedPhone, '-', 7, 0);
        }
        else {
            $treatedPhone = false;
        }
        
        if ($treatedPhone && (substr($treatedPhone, 0, 1) === '8' || substr($treatedPhone, 0, 1) === '9')) {
            $treatedPhone = '1-'.$treatedPhone;
        }
        
        return $treatedPhone;
    }

    static function iconvArrFromAcombaToUTF(array $p_resultArr)  {
        foreach ($p_resultArr as $key=>$value) {
            if (is_string($value)) {
                $p_resultArr[$key] = iconv("Windows-1252", "UTF-8", $value);
            } elseif (is_array($value)) {
                $p_resultArr[$key] = self::iconvArrFromAcombaToUTF($value);
            }
        }
        return $p_resultArr;
    }

    static function iconvArrFromUTFToAcomba(array $p_resultArr)  {
        foreach ($p_resultArr as $key=>$value) {
            if (is_string($value)) {
                $p_resultArr[$key] = iconv( "UTF-8", "Windows-1252", $value);
            } elseif (is_array($value)) {
                $p_resultArr[$key] = self::iconvArrFromUTFToAcomba($value);
            }
        }
        return $p_resultArr;
    }
}
