<?php
declare(strict_types=1);

use App\Application\Actions\Client\ViewClientAction;
use App\Application\Actions\Product\SyncProductsWithSiteAction;
use App\Application\Actions\Product\ViewAllProductsAction;
use App\Application\Actions\Product\ViewProductAction;
use App\Application\Actions\SiteOrder\ViewNewSiteOrdersAction;
use App\Application\Actions\SiteOrder\ViewSiteOrderAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Application\SiteApi\Client as SiteApiClient;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/clients', function (Group $group) {
        $group->get('/{id:[0-9\-]+}', ViewClientAction::class);
    });

    $app->group('/products', function (Group $group) {
        $group->get('', ViewAllProductsAction::class);
        $group->get('/sync', SyncProductsWithSiteAction::class);
        $group->get('/site_id_reference', function (Request $request, Response $response) {
            // Get Products in Site DB
            $siteApi = $this->get(SiteApiClient::class); 
            $productsInSite = $siteApi->get('products/all_skus_with_ids');

            $response->getBody()->write(json_encode($productsInSite));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        });
        $group->get('/site/{id}', function (Request $request, Response $response, array $args) {
            // Get Products in Site DB
            $siteApi = $this->get(SiteApiClient::class); 
            $siteProduct = $siteApi->get('products/'.$args['id']);
            #var_dump($siteProduct); exit;

            $response->getBody()->write(json_encode($siteProduct));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        });
        $group->get('/{number:[0-9A-Za-z\-]+}', ViewProductAction::class);
    });

    $app->get('/parse_mid_ops', function (Request $request, Response $response) {
        $handle = fopen("mid_ops.txt", "r");
        $midOps = [];
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $siteMarkerPos = strpos($line, 'SITE :');
                $skuMarkerPos = strpos($line, 'SKU :');
                $siteSkuMarkerPos = strpos($line, '- -');
                $acombaSku = trim(substr($line, $skuMarkerPos+5));
                $site = trim(substr($line, $siteMarkerPos+6, $skuMarkerPos-$siteMarkerPos-6));;
                $siteSku = trim(substr($line, 0, $siteSkuMarkerPos));
                if ($acombaSku && $site != 'PARENT') {
                    $midOps[] = [
                        #'line'=>$line, 
                        'siteSku'=>$siteSku, 'site'=>$site, 'acombaSku'=>$acombaSku
                    ];
                }
            }

            fclose($handle);
        } else {
            die('Mid ops for SKUs file missing');
        } 
        $response->getBody()->write(json_encode($midOps));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    });

    $app->group('/site_orders', function (Group $group) {
        $group->get('/new', ViewNewSiteOrdersAction::class);
        $group->get('/{id:[0-9]+}', ViewSiteOrderAction::class);
    });

    /*
    $app->get('/test_invoice', function (Request $request, Response $response) {
        $woocommerce = new Client(
            'https://dev.cardiochoc.ca', 
            'ck_f840d7ec6c5af7e6a2443eba4dcf6f06f4db5d4a', 
            'cs_dca829d04529746c581139cb9f89cff2945209ed',
            [
                'version' => 'wc/v3',
            ]
        );

        $order = $woocommerce->get('orders/6583');

        $response->getBody()->write(json_encode($order));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);

        $acombaDb = $this->get(PDO::class);

        // Let's start the new order!
        $acombaDb->exec('BEGIN_TRANSACTION_IN');

        ////////////////////////////
        // Init invoice header props

        $insertTrxHeadStmt = $acombaDb->prepare(
            'INSERT INTO TransactionHeader 
                (InInvoiceType, InReference, InDescription, InCurrentDay, InTransactionActive, InCustomerSupplierNumber, InTaxGroupCP, TANumLines) 
            VALUES 
                (1, :reference, :description, 1, 1, :id_client, :lines)'
        );
        $insertTrxHeadStmt->bindValue(':reference', 'WEB-'.$order['id']);
        $insertTrxHeadStmt->bindValue(':description', 'Commande du site');
        $insertTrxHeadStmt->bindValue(':id_client', '514-123-1234');
        $insertTrxHeadStmt->bindValue(':lines', 4); // !!!! MAke sure this is updated properly with sync
        //$insertTrxHeadStmt->execute();

        ////////////////////////////
        // Init invoice lines (products, shipping, taxes)
        $updateTrxLineStmt = $acombaDb->prepare(
            'UPDATE TransactionDetail SET 
                ILType = 1, ILLineNumber = 1, ILProductNumber = :product_number,
                ILDescription = :description, ILSellingPrice = :price, 
                ILProductGroupCP = 1, ILInvoicedQty = 1 
            WHERE TaNum=1"'
        );
        $updateTrxLineStmt->bindValue(':product_number', 'S99425-000097');
        $updateTrxLineStmt->bindValue(':description', 'Défibrillateur (DEA) LIFEPAK 1000');
        $updateTrxLineStmt->bindValue(':price', 2925.00);
        $updateTrxLineStmt->execute();

        $updateTrxLineStmt = $acombaDb->prepare(
            'UPDATE TransactionDetail SET 
                ILType = 1, ILLineNumber = 2, ILProductNumber = :product_number,
                ILDescription = :description, ILSellingPrice = :price, 
                ILProductGroupCP = 1, ILInvoicedQty = 1 
            WHERE TaNum=2"'
        );
        $updateTrxLineStmt->bindValue(':product_number', 'FT');
        $updateTrxLineStmt->bindValue(':description', 'Transport and handling fees');
        $updateTrxLineStmt->bindValue(':price', 11,43);

        // TODO START HERE!!!!
    
        $updateTrxLineStmt = $acombaDb->prepare(
            'UPDATE TransactionDetail SET 
                ILType = 1, ILLineNumber = 2, ILProductNumber = :product_number,
                ILDescription = :description, ILSellingPrice = :price, 
                ILProductGroupCP = 1, ILInvoicedQty = 1 
            WHERE TaNum=2"'
        );
        $updateTrxLineStmt->bindValue(':product_number', 'FT');
        $updateTrxLineStmt->bindValue(':description', 'Transport and handling fees');
        $updateTrxLineStmt->bindValue(':price', 11,43);

        // Let's start the new order!
        $acombaDb->exec('END_TRANSACTION_IN');
        

        $response->getBody()->write(json_encode(['OK']));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }); 
    */

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write(json_encode(["Acomba/WooCommerce Sync API","Acomba Sync API"]));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    });
};
