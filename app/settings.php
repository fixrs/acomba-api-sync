<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'acomba_odbc'   => [
                    # D:\DATA\DATA\F1000.DTA\CARDIO CHOC
                    #'acombapath' => 'C:\Fortune;Dbq=Z:\DATA\F1000.DTA\CARDIO CHOC',
                    'acombapath' => iconv('UTF-8', 'ISO-8859-1', 'C:\Fortune;Dbq=D:\1. Donn�es\5. Acomba\F1000.DTA\CARDIO CHOC'),
                    'username' => 'GAGMA',
                    'password' => '123ABC!!'
                ],
                'site_api'   => [
                    #'url' => 'https://dev.cardiochoc.ca/sync-api/public',
                    #'url' => 'http://cc_wp_sync.localhost',
                    'url' => 'http://cc_wp_sync_api_php',
                    'token' => 'kx9PdQTHsbDNjfASwj2KY5JhnMVWDucsA9ZqQjwkqrFkAaSucvpypZR44msVmtZ52f5aY2YZ8bxcYxvNRPkq2ckZm6ccSuk2v4UARkZnrBXfQv9r4EuqkGqcJMuqF6Kz'
                ],
                #'site_wc_api'   => [
                #    'url' => 'https://dev.cardiochoc.ca',
                #    'key' => 'ck_f840d7ec6c5af7e6a2443eba4dcf6f06f4db5d4a',
                #    'secret' => 'cs_dca829d04529746c581139cb9f89cff2945209ed'
                #],
                'site_wc_api'   => [
                    'url' => 'http://cardiochoc_php',
                    'key' => 'ck_f840d7ec6c5af7e6a2443eba4dcf6f06f4db5d4a',
                    'secret' => 'cs_dca829d04529746c581139cb9f89cff2945209ed'
                ],
            ]);
        }
    ]);
};
