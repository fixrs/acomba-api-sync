<?php
declare(strict_types=1);

use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use App\Application\SiteApi\Client as SiteApiClient;
use Automattic\WooCommerce\Client as SiteWCApiClient;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        PDO::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);
 
            $dbSettings = $settings->get('acomba_odbc');
            $acombapath = $dbSettings['acombapath'];
            $username = $dbSettings['username'];
            $password = $dbSettings['password'];
            $dsn = "odbc:Driver={Acomba ODBC Driver};AcombaExe=$acombapath";

            try {
                $db = new PDO(
                    $dsn, 
                    $username, 
                    $password
                );
              } catch (PDOException $exception) {
                echo $exception->getMessage();
                exit;
            }
            
            return $db;
        },

        SiteApiClient::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);
 
            $settings = $settings->get('site_api');
            $url = $settings['url'];
            $token = $settings['token'];

            return new SiteApiClient($url, $token);
        },

        SiteWCApiClient::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);
 
            $settings = $settings->get('site_wc_api');
            $url = $settings['url'];
            $key = $settings['key'];
            $secret = $settings['secret'];

            return new SiteWCApiClient(
                $url, $key, $secret, [ 'version' => 'wc/v3', 'timeout' => 60 ]
            );
        },
    ]);
};
